package com.free2move.animateclockapp;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;


public class AnimatedClockView extends View {


    private float mAngle;
    private RectF mRect = new RectF();

    public AnimatedClockView(Context context) {
        super(context);
        init(null, 0);
    }

    public AnimatedClockView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0);
    }


    public AnimatedClockView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs, defStyle);
    }

    private void init(AttributeSet attrs, int defStyle) {
        // Load attributes
        final TypedArray a = getContext().obtainStyledAttributes(
                attrs, R.styleable.AnimatedClockView, defStyle, 0);

        mAngle = a.getFloat(R.styleable.AnimatedClockView_angle, 0);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        int size = Math.min(getWidth(), getHeight());
        mRect.set(0, 0, size, size);
        ClockStyleKit.drawClockView(canvas,
                                    mRect,
                                    ClockStyleKit.ResizingBehavior.AspectFill,
                                    mAngle);

    }

    public void setAngle(float angle) {
        this.mAngle = angle;
        invalidate();
    }
}
