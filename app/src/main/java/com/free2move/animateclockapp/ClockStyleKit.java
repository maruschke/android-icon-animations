package com.free2move.animateclockapp;

import android.graphics.Paint;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Path;
import java.util.Stack;



/**
 * Created by AuthorName on 14.06.17.
 * Copyright © 2017 CompanyName. All rights reserved.
 *
 * This code was generated by Trial version of PaintCode, therefore cannot be used for commercial purposes.
 * http://www.paintcodeapp.com
 *
 * @author AuthorName
 */
public class ClockStyleKit {


    // Resizing Behavior
    public enum ResizingBehavior {
        AspectFit, //!< The content is proportionally resized to fit into the target rectangle.
        AspectFill, //!< The content is proportionally resized to completely fill the target rectangle.
        Stretch, //!< The content is stretched to match the entire target rectangle.
        Center, //!< The content is centered in the target rectangle, but it is NOT resized.
    }

    // In Trial version of PaintCode, the code generation is limited to 3 canvases.

    // Canvas Drawings
    // Tab

    private static class CacheForClockView {
        private static Paint paint = new Paint();
        private static RectF originalFrame = new RectF(0f, 0f, 603f, 603f);
        private static RectF resizedFrame = new RectF();
        private static RectF clockFrameRect = new RectF();
        private static Path clockFramePath = new Path();
        private static RectF clockFrame2Rect = new RectF();
        private static Path clockFrame2Path = new Path();
    }

    public static void drawClockView(Canvas canvas, float angle) {
        ClockStyleKit.drawClockView(canvas, new RectF(0f, 0f, 603f, 603f), ResizingBehavior.AspectFit, angle);
    }

    public static void drawClockView(Canvas canvas, RectF targetFrame, ResizingBehavior resizing, float angle) {
        // General Declarations
        Stack<Matrix> currentTransformation = new Stack<Matrix>();
        currentTransformation.push(new Matrix());
        Paint paint = CacheForClockView.paint;

        // Local Colors
        int color3 = Color.argb(134, 255, 38, 0);
        int color4 = Color.argb(255, 155, 169, 166);

        // Resize to Target Frame
        canvas.save();
        RectF resizedFrame = CacheForClockView.resizedFrame;
        ClockStyleKit.resizingBehaviorApply(resizing, CacheForClockView.originalFrame, targetFrame, resizedFrame);
        canvas.translate(resizedFrame.left, resizedFrame.top);
        canvas.scale(resizedFrame.width() / 603f, resizedFrame.height() / 603f);

        // ClockFrame
        canvas.save();
        canvas.translate(300.5f, 300.5f);
        currentTransformation.peek().postTranslate(300.5f, 300.5f);
        canvas.rotate(-90f);
        currentTransformation.peek().postRotate(-90f);
        RectF clockFrameRect = CacheForClockView.clockFrameRect;
        clockFrameRect.set(-300f, -300f, 300f, 300f);
        Path clockFramePath = CacheForClockView.clockFramePath;
        clockFramePath.reset();
        clockFramePath.addOval(clockFrameRect, Path.Direction.CW);

        paint.reset();
        paint.setFlags(Paint.ANTI_ALIAS_FLAG);
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(color3);
        canvas.drawPath(clockFramePath, paint);
        canvas.restore();

        // ClockFrame 2
        canvas.save();
        canvas.translate(300.5f, 300.5f);
        currentTransformation.peek().postTranslate(300.5f, 300.5f);
        canvas.rotate(-90f);
        currentTransformation.peek().postRotate(-90f);
        RectF clockFrame2Rect = CacheForClockView.clockFrame2Rect;
        clockFrame2Rect.set(-300f, -300f, 300f, 300f);
        Path clockFrame2Path = CacheForClockView.clockFrame2Path;
        clockFrame2Path.reset();
        clockFrame2Path.addArc(clockFrame2Rect, -angle, angle + (0f < -angle ? 360f * (float) Math.ceil((-angle) / 360f) : 0f));
        clockFrame2Path.lineTo(clockFrame2Rect.centerX(), clockFrame2Rect.centerY());
        clockFrame2Path.close();

        paint.reset();
        paint.setFlags(Paint.ANTI_ALIAS_FLAG);
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(color4);
        canvas.drawPath(clockFrame2Path, paint);

        paint.reset();
        paint.setFlags(Paint.ANTI_ALIAS_FLAG);
        paint.setStrokeWidth(0.5f);
        paint.setStrokeMiter(10f);
        canvas.save();
        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(Color.GRAY);
        canvas.drawPath(clockFrame2Path, paint);
        canvas.restore();
        canvas.restore();

        canvas.restore();
    }


    // Resizing Behavior
    public static void resizingBehaviorApply(ResizingBehavior behavior, RectF rect, RectF target, RectF result) {
        if (rect.equals(target) || target == null) {
            result.set(rect);
            return;
        }

        if (behavior == ResizingBehavior.Stretch) {
            result.set(target);
            return;
        }

        float xRatio = Math.abs(target.width() / rect.width());
        float yRatio = Math.abs(target.height() / rect.height());
        float scale = 0f;

        switch (behavior) {
            case AspectFit: {
                scale = Math.min(xRatio, yRatio);
                break;
            }
            case AspectFill: {
                scale = Math.max(xRatio, yRatio);
                break;
            }
            case Center: {
                scale = 1f;
                break;
            }
        }

        float newWidth = Math.abs(rect.width() * scale);
        float newHeight = Math.abs(rect.height() * scale);
        result.set(target.centerX() - newWidth / 2,
                   target.centerY() - newHeight / 2,
                   target.centerX() + newWidth / 2,
                   target.centerY() + newHeight / 2);
    }


}