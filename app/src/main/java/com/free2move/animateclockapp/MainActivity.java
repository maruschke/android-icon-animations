package com.free2move.animateclockapp;

import android.support.annotation.RequiresApi;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//                final AnimatedClockView view = (AnimatedClockView) findViewById(R.id.animateClockview);

//                ValueAnimator animator = ValueAnimator.ofFloat(-90f, 0f);
//                animator.setDuration(10000);
//                animator.setInterpolator(new LinearInterpolator());
//                animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
//                    @Override
//                    public void onAnimationUpdate(ValueAnimator animation) {
//                        view.setAngle((Float) animation.getAnimatedValue());
//                    }
//                });
//                animator.setRepeatCount(ValueAnimator.INFINITE);
//                animator.start();

        ImageView vector = (ImageView) findViewById(R.id.clock_vector_view);
        final AnimatedVectorDrawable drawable = (AnimatedVectorDrawable) vector.getDrawable();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            vector.setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void onClick(View v) {
                    drawable.start();
                }
            });
        }
    }
}
